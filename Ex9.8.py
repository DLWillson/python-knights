#!/usr/bin/python
# odo length = 6
# right(odo,4) is palindromic
# right(odo+1,5) is palindromic
# middle(odo+2,4) is palindromic
# odo+3 is palindromic

def zeropad(strnum,length):
  if len(strnum) < length:
    return "0" * ( length - len(strnum) ) + strnum
  return strnum

def strodo(odo_num, odo_length=6):
  return zeropad(str(odo_num), odo_length)

def reverse(word):
  if len(word) < 2:
    return word
  backword=''
  for c in range(len(word)-1,-1,-1):
    backword+=word[c]
  return(backword)

def is_palindrome(word):
  if word == reverse(word):
    return True
  return False

for odo in range(999999):
##  print(strodo)
##  print(reverse(strodo))
##  print(is_palindrome(strodo))
  # rightmost 4 of odo is palindromic
  # rightmost 5 of odo + 1 is palindromic
  # middle 4 of odo + 2 is palindromic
  # odo + 3 is palindromic

  if (
    is_palindrome(strodo(odo)[2:]) and
    is_palindrome(strodo(odo+1)[1:]) and
    is_palindrome(strodo(odo+2)[1:5]) and
    is_palindrome(strodo(odo+3))
    ):
    print(strodo(odo) + " is palindromic on the right-side 4")
    print(strodo(odo+1) + " is palindromic on the right-side 5")
    print(strodo(odo+2) + " is palindromic in the middle 4")
    print(strodo(odo+3) + " is palindromic")
