#!/usr/bin/python
# for the Python Knights Study Group in 2016, led by Matt James

# Pre-requisites in my Fedora box:
#   sudo pip install swampy
#   sudo dnf install tkinter

from swampy.TurtleWorld import *

def draw_tree(t, length, n):
    if n == 0:
        return
    angle = 50
    fd(t, length*n)
    lt(t, angle)
    draw(t, length, n-1)
    rt(t, 2*angle)
    draw(t, length, n-1)
    lt(t, angle)
    bk(t, length*n)

world = TurtleWorld()
david = Turtle()
david.delay=0.05

draw(david, 5, 5)
