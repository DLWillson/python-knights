#!/usr/bin/python
def uses_only(word,uses):
  for char in word:
    if not char in uses:
      return False
  return True

fil = open('words.txt')
#fil = open('little_words')
lines = 0
lines_with_only = 0
print("Which characters should be used? ")
uses = raw_input()

for lin in fil:
  lin = lin.strip()
  lines += 1
  if uses_only(lin,uses):
    print(lin)
    lines_with_only += 1

print(str(lines) + " lines.")
print(str(lines_with_only) + " lines with only " + uses)
print(str( lines_with_only * 100 / lines ) + "% lines with only those chars")
