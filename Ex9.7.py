#!/usr/bin/python
# three doubled letters
def three_doubles(word):
  if len(word) < 6:
    return False
  for i in range(0,len(word)-6):
    if word[i]==word[i+1] and word[i+2]==word[i+3] and word[i+4]==word[i+5]:
      print(word + " contains three doubles!")
      return True
  return False

fil = open('words.txt')
#fil = open('little_words')
lines = 0
lines_w3d = 0

for lin in fil:
  lin = lin.strip()
  lines += 1
  if three_doubles(lin):
    print(lin)
    lines_w3d += 1

print(str(lines) + " lines")
print(str(lines_w3d) + " with three doubles")
print(str( lines_w3d * 100 / lines ) + "% with three doubles")
