print("This program checks four values against Fermat's Last Theorem, which supposes that for any value of n greater than two, there are no values of a, b, and c that satisfy the expression: a^n + b^n = c^n.")

def check_fermat(a,b,c,n):

    long_string_expression=a+'^'+n+'+'+b+'^'+n+'='+c+'^'+n
    print(long_string_expression)

    if not(int(n) > 2):
        print("Fermat don't give a shit about your little n.")
        return False

    short_string_expression=str(int(a)^int(n))+'+'+str(int(b)^int(n))+'='+str(int(c)^int(n))
    print(short_string_expression)
    left_side=int(a)^int(n)+int(b)^int(n)
    right_side=int(c)^int(n)
    if left_side == right_side:
        print(str(left_side)+'='+str(right_side)+'!')
        print("Holy smokes! Fermat was wrong!")
    else:
        print(str(left_side)+'<>'+str(right_side))
        print("It seems Fermat was right, in this case.")

a=raw_input("Enter a value for a: ")
b=raw_input("Enter a value for b: ")
c=raw_input("Enter a value for c: ")
n=raw_input("Enter a value for n: ")

check_fermat(a,b,c,n)
