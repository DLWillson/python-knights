#!/usr/bin/python
# Python Knights Practice by David L. Willson
# Exercise 6.1

def compare(x,y):
    """returns 1 if x greater than y, -1 if y is greater than x, 0 otherwise"""
    if x>y:
        return 1
    if y>x:
        return -1
    return 0

print(str(compare(4,2)))
print(str(compare(2,4)))
print(str(compare(4,4)))
print(str(compare(4000,2000)))
print(str(compare(2,2)))
