#!/usr/bin/python
# Flowers and Pies
# Still Turtling by David L. Willson
# for the Python Knights Study Group in 2016, led by Matt James

# Pre-requisites in my Fedora box:
#   sudo pip install swampy
#   sudo dnf install tkinter

import math
from swampy.TurtleWorld import *

def polylines(turtle,line_length,turn_degrees,num_lines):
    for s in range(num_lines):
        fd(turtle,line_length)
        lt(turtle,turn_degrees)

def polygon(turtle,sides,length):
    turn_degrees=float(360)/float(sides)
    polylines(turtle,length,turn_degrees,sides)

def rpolygon(turtle,sides,length):
    turn_degrees=float(360)/float(sides)*-1
    polylines(turtle,length,turn_degrees,sides)

def triangle(turtle,side_length):
    polygon(turtle,3,side_length)

def rtriangle(turtle,side_length):
    rpolygon(turtle,3,side_length)

def square(turtle,side_length):
    polygon(turtle,4,side_length)

def rsquare(turtle,side_length):
    rpolygon(turtle,4,side_length)

def hexagon(turtle,side_length):
    polygon(turtle,6,side_length)

def rhexagon(turtle,side_length):
    rpolygon(turtle,6,side_length)

def arc(turtle,radius,degrees):
    old_turtle_delay = turtle.delay
    turtle.delay = 0
    circumference=2.0 * math.pi * float(radius)
    arc_length=abs(circumference * float(degrees) / 360.0)
    alpd=abs(arc_length/float(degrees))
    dpal=float(degrees)/arc_length
    if alpd > abs(dpal):
        if degrees > 0:
            polylines(turtle,alpd,1,degrees)
        else:
            polylines(turtle,alpd,-1,abs(degrees))
    else:
        polylines(turtle,1,dpal,int(arc_length+0.5))
    turtle.delay = old_turtle_delay

def circle(turtle,radius):
    arc(turtle,radius,360)

def rcircle(turtle,radius):
    arc(turtle,radius,-360)

def petal(turtle,radius,degrees):
    rt( turtle, float( degrees ) / 2 )
    arc(turtle,radius,degrees)
    lt( turtle, 180 - degrees )
    arc( turtle, radius, degrees )
    lt ( turtle, 180 - degrees / 2 )

def flower(turtle,radius,degrees):
    """
    flower draws a flower determining the number of petals
    from the degrees of the petal
        turtle is a turtle
        petals is the number of petals
    """
    petals = 360/degrees
    for p in range(petals):
        petal(turtle, radius, degrees)
        lt(turtle, degrees)

def megaflower(turtle,radius,degrees,petals):
    """
    megaflower draws a flower with an arbitrary number of petals
        turtle is a turtle
        petals is the number of petals
    """
    for p in range(petals):
        petal(turtle, radius, degrees)
        lt( turtle, 360/petals )

def pie(turtle,sections,size):
    inner_angle_degrees=360.0/float(sections)
    outer_angle_degrees=(180.0-inner_angle_degrees)/2.0
    outer_side_length=float(size) * math.cos( math.radians( outer_angle_degrees )) * 2
    for i in range(sections):
        fd( turtle, size )
        lt( turtle, 180 - outer_angle_degrees )
        fd( turtle, outer_side_length )
        lt( turtle, 180 - outer_angle_degrees )
        fd( turtle, size )
        lt( turtle, 180 )

world = TurtleWorld()
charlie = Turtle()
charlie.delay = 0.2

pu(charlie)
bk(charlie, 100)
lt(charlie)
fd(charlie, 100)
rt(charlie)
pd(charlie)

pie(charlie, 6, 60)
pie(charlie, 3, 60)

pu(charlie)
rt(charlie)
fd(charlie, 160)
lt(charlie)
pd(charlie)

pie(charlie, 4, 80)
rt(charlie, 45)
pie(charlie, 4, 80)
lt(charlie, 45)
pie(charlie, 8, 40)

pu(charlie)
fd(charlie, 230)
pd(charlie)

flower(charlie,100,90)
lt(charlie,45)
flower(charlie,60,90)
rt(charlie,45)
megaflower(charlie,60,45,16)

for x in range(360):
    lt(charlie,5)

wait_for_user()
