#!/usr/bin/python

def avoids(word,avoid):
  for char in avoid:
    if char in word:
      return False
  return True

fil = open('words.txt')
lines = 0
lines_without_avoid = 0
print("Which characters should be avoided? ")
avoid = raw_input()

for lin in fil:
  lin = lin.strip()
  lines += 1
  if avoids(lin,avoid):
    lines_without_avoid += 1

print(str(lines) + " lines.")
print(str(lines_without_avoid) + " lines without " + avoid)
print(str( lines_without_avoid * 100 / lines ) + "% lines avoid")
