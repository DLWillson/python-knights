#!/usr/bin/python
# Letters
# Still Turtling by David L. Willson
# for the Python Knights Study Group in 2016, led by Matt James

# Pre-requisites in my Fedora box:
#   sudo pip install swampy
#   sudo dnf install tkinter

import math
from swampy.TurtleWorld import *

def polylines(turtle,line_length,turn_degrees,num_lines):
    for s in range(num_lines):
        fd(turtle,line_length)
        lt(turtle,turn_degrees)

def polygon(turtle,sides,length):
    turn_degrees=float(360)/float(sides)
    polylines(turtle,length,turn_degrees,sides)

def rpolygon(turtle,sides,length):
    turn_degrees=float(360)/float(sides)*-1
    polylines(turtle,length,turn_degrees,sides)

def triangle(turtle,side_length):
    polygon(turtle,3,side_length)

def rtriangle(turtle,side_length):
    rpolygon(turtle,3,side_length)

def square(turtle,side_length):
    polygon(turtle,4,side_length)

def rsquare(turtle,side_length):
    rpolygon(turtle,4,side_length)

def hexagon(turtle,side_length):
    polygon(turtle,6,side_length)

def rhexagon(turtle,side_length):
    rpolygon(turtle,6,side_length)

def arc(turtle,radius,degrees):
    old_turtle_delay = turtle.delay
    turtle.delay = 0
    circumference=2.0 * math.pi * float(radius)
    arc_length=abs(circumference * float(degrees) / 360.0)
    alpd=abs(arc_length/float(degrees))
    dpal=float(degrees)/arc_length
    if alpd > abs(dpal):
        if degrees > 0:
            polylines(turtle,alpd,1,degrees)
        else:
            polylines(turtle,alpd,-1,abs(degrees))
    else:
        polylines(turtle,1,dpal,int(arc_length+0.5))
    turtle.delay = old_turtle_delay

def circle(turtle,radius):
    arc(turtle,radius,360)

def rcircle(turtle,radius):
    arc(turtle,radius,-360)

def skip(turtle,size):
    pu(turtle)
    fd(turtle,size)
    pd(turtle)

def draw_a(turtle,size):
    lt(turtle,60)
    fd(turtle,size*0.2)
    rt(turtle,60)
    triangle(turtle,size*0.6)
    fd(turtle,size*0.6)
    rt(turtle,60)
    fd(turtle,size*0.2)
    lt(turtle,60)

def draw_b(turtle,size):
    arc(turtle,float(size)*0.3,180)
    lt(turtle,180)
    arc(turtle,float(size)*0.2,180)
    lt(turtle)
    fd(turtle,size)
    lt(turtle)
    pu(turtle)
    fd(turtle,size)
    pd(turtle)

def draw_c(turtle,size):
    pu(turtle)
    lt(turtle)
    fd(turtle,size)
    rt(turtle)
    fd(turtle,size)
    lt(turtle,180)
    pd(turtle)
    arc(turtle,int(float(size)*0.5+0.5),180)

def draw_d(turtle,size):
    arc(turtle,int(float(size)*0.5+0.5),180)
    lt(turtle)
    fd(turtle,size)
    lt(turtle)
    pu(turtle)
    fd(turtle,size)
    pd(turtle)

def draw_e(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_f(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_g(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_h(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_i(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_j(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_k(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_l(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_m(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_n(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_o(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_p(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_q(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_r(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_s(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_t(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_u(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_v(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_w(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_x(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_y(turtle,size):
    square(turtle,size)
    fd(turtle,size)
def draw_z(turtle,size):
    square(turtle,size)
    fd(turtle,size)
