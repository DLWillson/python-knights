#!/usr/bin/python
# Kochy Turtle Practice by David L. Willson
# for the Python Knights Study Group in 2016, led by Matt James

# Pre-requisites in my Fedora box:
#   sudo pip install swampy
#   sudo dnf install tkinter

from swampy.TurtleWorld import *

def koch(t, size, seg):
    if size < seg:
        fd(t,size)
        return
    len=size/3
    koch(t,len,seg)
    lt(t,60)
    koch(t,len,seg)
    rt(t,120)
    koch(t,len,seg)
    lt(t,60)
    koch(t,len,seg)

def snowflake(t, size, seg, sides):
    turned=0
    turn=360/sides
    while turned < 360:
        koch(t, size, seg)
        rt(t,turn)
        turned+=turn

world = TurtleWorld()
david = Turtle()
david.delay=0.0

# snowflake(david, 360, 10, 3)
snowflake(david, 120, 10, 6)
