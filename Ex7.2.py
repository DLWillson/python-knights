#!/usr/bin/python
# for SFS Python Knights by Matt James
# by David L. Willson
# Exercise 7.2

def square_root(square):
    print("square="+str(square))
    epsilon=0.001
    print("epsilon="+str(epsilon))
    magnitude=1
    while 10**magnitude < square:
        magnitude+=1
    print("mag="+str(magnitude))
    first_guess=10**(magnitude/2)
    print("first guess: " + str(first_guess))
    cur_guess=float(first_guess)
    while abs( cur_guess**2.0 - float(square) ) > epsilon:
        print("rejected guess:" + str(cur_guess))
        new_guess=(cur_guess+square/cur_guess)/2
        print("new guess:" + str(new_guess))
        cur_guess=new_guess
        
    return cur_guess

for square in [10,10000,1000000,42,38,107,81]:
    print("The square root of " + str(square) + " is " + str(square_root(square)) + ".")
