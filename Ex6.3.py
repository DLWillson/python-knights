#!/usr/bin/python
# Python Knights Practice by David L. Willson
# Exercise 6.2
from math import sqrt

def is_between(x,y,z):
    """returns len of hypotenuse for sides a, b"""
    return x <= y and y <= z

print(str(is_between(3,4,5)))
print(str(is_between(0,0,0)))
print(str(is_between(4,3,4)))
print(str(is_between(1,1,2)))
