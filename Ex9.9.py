#!/usr/bin/python
# me-mom palindromic ages
def reverse(word):
  if len(word) < 2:
    return word
  backword=''
  for c in range(len(word)-1,-1,-1):
    backword+=word[c]
  return(backword)


for gap in range(1,121):
  occurrences=0
  for me in range(1,121):
    mom = me + gap
    if mom > 120:
      # print("Mom died.")
      break
    zeroes='0' * (len(str(mom)) - len(str(me)))
    if str(mom)==reverse(zeroes+str(me)):
      occurrences+=1
      if occurrences > 5:
        print("Mom is " + str(mom) + ", and I'm " + str(me) + ".")
  if occurrences > 5:
    print("I died with " + str(occurrences) + " me-mom palindromic ages. Mom was " + str(gap) + " years older than me.")
