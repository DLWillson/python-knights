def nfind(word,letter,start_pos):
    index = start_pos
    while index < len(word):
        if word[index] == letter:
            return index
        index = index + 1
    return -1
  
def count(needle,haystack):
  print("haystack="+haystack)
  print("needle="+needle)
  count=0
  pos=0
  while True:
    pos=nfind(haystack,needle,pos)+1
    if pos == 0:
      break
    else:
      count += 1
  return count

print(str(count('a','banana')))
print(str(count('a','bananaaaa')))
print(str(count('c','bananaaaa')))
print(str(count('c','canard')))

