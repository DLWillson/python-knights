#!/usr/bin/python
def uses_only(word,uses):
  for char in word:
    if not char in uses:
      return False
  return True

def uses_all(word,uses):
  for char in uses:
    if not char in word:
      return False
  return True

fil = open('words.txt')
#fil = open('little_words')
lines = 0
lines_with_all = 0
print("Which characters should be used? ")
uses = raw_input()

for lin in fil:
  lin = lin.strip()
  lines += 1
  if uses_all(lin,uses):
    print(lin)
    lines_with_all += 1

print(str(lines) + " lines.")
print(str(lines_with_all) + " lines with all " + uses)
print(str( lines_with_all * 100 / lines ) + "% lines with all those chars")
