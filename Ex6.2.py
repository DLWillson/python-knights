#!/usr/bin/python
# Python Knights Practice by David L. Willson
# Exercise 6.2
from math import sqrt

def hypotenuse(a,b):
    """returns len of hypotenuse for sides a, b"""
    square_a=float(a)**2.0
    square_b=float(b)**2.0
    square_c=square_a+square_b
##    print("square_c="+str(square_c))
    return(sqrt(square_c))

print(str(hypotenuse(3,4)))
print(str(hypotenuse(0,0)))
print(str(hypotenuse(4,3)))
print(str(hypotenuse(1,1)))
print(str(hypotenuse(sqrt(2),sqrt(2))))
