def find(word, letter):
    index = 0
    while index < len(word):
        if word[index] == letter:
            return index
        index = index + 1
    return -1

def nfind(word,letter,start_pos):
    index = start_pos
    while index < len(word):
        if word[index] == letter:
            return index
        index = index + 1
    return -1

print(str(find('banana','a')))
print(str(nfind('banana','a',3)))
