#!/usr/bin/python
# Palindrome
# by David L. Willson
# for the Python Knights Study Group in 2016, led by Matt James

def is_palindrome(string_1,string_2):
	if string_1 == string_2[::-1]:
		return True
	return False

print(str(is_palindrome('drow','word')))
print(str(is_palindrome('god','dog')))
print(str(is_palindrome('slipped','ellipsis')))
print(str(is_palindrome('monkey','dingo')))

