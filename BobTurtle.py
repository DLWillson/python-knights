#!/usr/bin/python
# Turtle Practice by David L. Willson
# for the Python Knights Study Group in 2016, led by Matt James

# Pre-requisites in my Fedora box:
#   sudo pip install swampy
#   sudo dnf install tkinter

from swampy.TurtleWorld import *
world = TurtleWorld()
bob = Turtle()

def square(turtle,length):
    for s in range(4):
        fd(turtle,length)
        lt(turtle)

def polygon(turtle,sides,length):
    degrees_per_turn=float(360)/float(sides)
    for s in range(sides):
        fd(turtle,length)
        lt(turtle,degrees_per_turn)

def circle(turtle,radius):
    circumference=int(2.0 * 3.14 * float(radius))
    polygon(turtle,circumference,1)

def arc(turtle,radius,degrees):
    circumference=int(2.0 * 3.14 * float(radius))
    arc_length=circumference * float(degrees)/360.0
    degrees_per_turn=float(degrees)/arc_length
    for s in range(int(arc_length)):
        fd(turtle,1)
        lt(turtle,degrees_per_turn)

bob.delay = 0.2

# Triangle
polygon(bob,3,100)

# Hexagon
polygon(bob,6,50)

bob.delay = 0

# small circle
circle(bob,25)

# medium half-circle
arc(bob,50,180)

# big circle
circle(bob,100)

bob.delay = 0.2

# square
polygon(bob,4,100)

wait_for_user()
