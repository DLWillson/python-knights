#!/usr/bin/python
# More Turtle Practice by David L. Willson
# for the Python Knights Study Group in 2016, led by Matt James

# Pre-requisites in my Fedora box:
#   sudo pip install swampy
#   sudo dnf install tkinter

import math
from swampy.TurtleWorld import *

world = TurtleWorld()
alice = Turtle()

def polylines(turtle,line_length,turn_degrees,num_lines):
    for s in range(num_lines):
        fd(turtle,line_length)
        lt(turtle,turn_degrees)

def polygon(turtle,sides,length):
    turn_degrees=float(360)/float(sides)
    polylines(turtle,length,turn_degrees,sides)

def rpolygon(turtle,sides,length):
    turn_degrees=float(360)/float(sides)*-1
    polylines(turtle,length,turn_degrees,sides)

def triangle(turtle,side_length):
    polygon(turtle,3,side_length)

def rtriangle(turtle,side_length):
    rpolygon(turtle,3,side_length)

def square(turtle,side_length):
    polygon(turtle,4,side_length)

def rsquare(turtle,side_length):
    rpolygon(turtle,4,side_length)

def hexagon(turtle,side_length):
    polygon(turtle,6,side_length)

def rhexagon(turtle,side_length):
    rpolygon(turtle,6,side_length)

def arc(turtle,radius,degrees):
    circumference=2.0 * math.pi * float(radius)
    arc_length=abs(circumference * float(degrees) / 360.0)
    alpd=abs(arc_length/float(degrees))
    dpal=float(degrees)/arc_length
    print("circumference=" + str(circumference))
    print("arc_length=" + str(arc_length))
    print("degrees=" + str(degrees))
    print("alpd=" + str(alpd))
    print("dpal=" + str(dpal))
    if alpd > abs(dpal):
        if degrees > 0:
            print("polylines(turtle,alpd,1,abs(degrees))")
            polylines(turtle,alpd,1,degrees)
        else:
            print("polylines(turtle,alpd,-1,degrees)")
            polylines(turtle,alpd,-1,abs(degrees))
    else:
        print("polylines(turtle,1,dpal,int(arc_length+0.5)")
        polylines(turtle,1,dpal,int(arc_length+0.5))
    print()
        
def circle(turtle,radius):
    arc(turtle,radius,360)

def rcircle(turtle,radius):
    arc(turtle,radius,-360)

# === show off a bit, Alice ===

# polygon stuph looks cooler with a long delay
alice.delay = 0.2

# Triangle
bk(alice,60)
triangle(alice,120)
fd(alice,60)

# Right-Hand Hexagon
bk(alice,30)
rhexagon(alice,60)
fd(alice,30)

# big right-hand square
pu(alice)
bk(alice,120)
lt(alice)
bk(alice,120)
pd(alice)
rsquare(alice,240)
pu(alice)
fd(alice,120)
rt(alice)
fd(alice,120)
pd(alice)

# circle stuph looks cooler with no delay
alice.delay = 0.0

# small half-circles
arc(alice,30,-180)
rt(alice)
fd(alice,120)
lt(alice)
arc(alice,30,180)

pu(alice)
rt(alice)
fd(alice,140)
rt(alice)
pd(alice)

# big circle
rcircle(alice,140)

wait_for_user()
