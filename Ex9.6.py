#!/usr/bin/python
def uses_only(word,uses):
  for char in word:
    if not char in uses:
      return False
  return True

def uses_all(word,uses):
  for char in uses:
    if not char in word:
      return False
  return True

def is_abecedarian(word):
  old_char=word[0]
  for char in word:
    if char < old_char:
      return False
    old_char=char
  return True

fil = open('words.txt')
#fil = open('little_words')
lines = 0
lines_abcd = 0

for lin in fil:
  lin = lin.strip()
  lines += 1
  if is_abecedarian(lin):
    print(lin)
    lines_abcd += 1

print(str(lines) + " lines")
print(str(lines_abcd) + " abecedarian lines")
print(str( lines_abcd * 100 / lines ) + "% abecedarian lines")
